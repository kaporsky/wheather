<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;
use App\Weather;
class WeatherUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weather parser yandex';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = (int)date("Y");
        $month = (int)date("m");
        $data = array();
        $day = 0;
        $html = $this->getWeather();
        include(__DIR__."/../../../vendor/phpQuery/phpQuery.php");
        $document = \phpQuery::newDocument($html);
        $days = $document->find("dl > dt");
        $data = array();
        foreach ($days as $d) {
            $pq = pq($d);
            $cur_day = (int)$pq->attr("data-anchor");
            if($cur_day<$day) {
                $month++;
                if($month>12) {
                    $year++;
                    $month = 1;
                }
            }
            $day = $cur_day;
            $date = implode(array($year, $month, $day), "-");
            $pq = pq($pq->next());
            $sunrise = $pq->find("div.forecast-detailed__sunrise div.forecast-detailed__value")->text();
            $sunset = $pq->find("div.forecast-detailed__sunset div.forecast-detailed__value")->text();
            $magnetic_icon = $pq->find("div.forecast-detailed__moon > i")->attr("class");
            preg_match("/(icon_moon_[0-9]+)/s", $magnetic_icon, $magnetic_icon);
            $magnetic_icon = $magnetic_icon[1];
            $magnetic_field = $pq->find("div.forecast-detailed__geomagnetic-field div.forecast-detailed__value")->text();
            $magnetic_field = (!empty($magnetic_field) ? $magnetic_field : "");
            $weather_table = $pq->find("table.weather-table > tbody > tr");
            $weather = array();
            foreach ($weather_table as $tr) {
                $weather_day = array();
                $pq = pq($tr);
                $weather_day["daypart"] = $pq->find("td.weather-table__body-cell_type_daypart div.weather-table__daypart")->text();
                $weather_day["temp"] = $pq->find("td.weather-table__body-cell_type_daypart div.weather-table__temp")->text();
                $type_icon = $pq->find("td.weather-table__body-cell_type_icon i.icon")->attr("class");
                preg_match("/(icon_thumb_.*)$/s", $type_icon, $type_icon);
                $weather_day["type_icon"] = $type_icon[1];
                $weather_day["condition"] = $pq->find("td.weather-table__body-cell_type_condition div.weather-table__value")->text();
                $weather_day["air_pressure"] = $pq->find("td.weather-table__body-cell_type_air-pressure div.weather-table__value")->text();
                $weather_day["type_humidity"] = $pq->find("td.weather-table__body-cell_type_humidity div.weather-table__value")->text();
                $pq = pq($pq->find("td.weather-table__body-cell_type_wind"));
                $weather_day["type_wind"]["abbr"] = $pq->find("abbr")->text();
                $icon_wind = $pq->find("i.icon_wind")->attr("class");
                preg_match("/(icon_wind_.*?)\ /s", $icon_wind, $icon_wind);
                $weather_day["type_wind"]["icon_wind"] = $icon_wind[1];
                $weather_day["type_wind"]["wind_speed"] = $pq->find("span.wind-speed")->text();
                $weather[] = $weather_day;
            }
            $weather = json_encode($weather);

            Weather::updateOrCreate(
               ['day' => $date],
               ['data' => $weather, 'sunrise' => $sunrise, 'sunset' => $sunset, 'magnetic_field' => $magnetic_field, 'magnetic_icon' => $magnetic_icon]
            );
        }
    }

    private function getWeather() {
        $ch = curl_init("https://yandex.ru/pogoda/saint-petersburg/details");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7");
        $result = curl_exec($ch);
        return $result;
    }
}
