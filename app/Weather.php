<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
	protected $table = 'weather';
    protected $fillable = ['day', 'data', 'sunrise', 'sunset', 'magnetic_field', 'magnetic_icon'];
}
