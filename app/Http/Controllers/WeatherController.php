<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Weather;

class WeatherController extends Controller
{
    public function showWeather() {
    	$now = date("Y-m-d");
    	$weather = Weather::where("day", ">=", $now)->orderBy("day", "asc")->get();
    	// dd($weather->toArray());
    	return view('weather', ['weather' => $weather, 'now' => $now]);
    }
}
