@extends('base')
@section('title')
	<title>Weather</title>
@endsection

@section('content')
	<div class="container">
		<table class="tabel table-striped table-bordered col-sm-12">
			<tbody>
				@foreach ($weather as $w)
				<tr>
					<td>{{$w->day}} @if($w->day==$now) Сегодня@endif</td>
					<td>
						<?php $data = json_decode($w->data); ?>
						<table class="table">
						<tbody>
							@foreach ($data as $d)
								<tr>
									<td>{{$d->daypart}}:<br />{{$d->temp}}</td>
									<td><i class="icon {{$d->type_icon}}"></i></td>
									<td>{{$d->condition}}</td>
									<td>{{$d->air_pressure}}</td>
									<td>{{$d->type_humidity}}</td>
									<td><i class="icon icon_wind {{$d->type_wind->icon_wind}}"></i><br />{{$d->type_wind->abbr}}</td>
									<td>{{$d->type_wind->wind_speed}}</td>
								</tr>
							@endforeach
						</tbody>
						</table>
					</td>
					<td>Восход:<br />{{$w->sunrise}}</td>
					<td>Закат:<br />{{$w->sunset}}</td>
					<td><i class="icon {{$w->magnetic_icon}}"></i></td>
					<td>@if($w->magnetic_field)Магнитное поле:<br />{{$w->magnetic_field}}@endif</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection