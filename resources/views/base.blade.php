<!DOCTYPE html>
<html lang="ru">
  <head>
    @yield('title')
    <link rel="stylesheet" type="text/css" href="css/app.css" />
    <link rel="stylesheet" type="text/css" href="css/yandex.css" />
    <script src="js/app.js"></script>
    <!-- CSS и JavaScript -->
  </head>

  <body>
    @include('menu')
    @yield('content')
  </body>
</html>